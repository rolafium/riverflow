
import os
from flow.version import VERSION

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'flowing',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'flowing.middleware.UserProfileMiddleware',
    'flowing.middleware.JsonMiddleware',
]

ROOT_URLCONF = 'flow.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'flowing.context_processors.versioning',
            ],
        },
    },
]

WSGI_APPLICATION = 'flow.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
REQUEST_EXPIRE_MINUTES = 24 * 60

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEBUG = False
ALLOWED_HOSTS = []
SECRET_KEY = ""
STATIC_URL = ""
DJANGO_ADMIN_SITE_URL = ""
DATABASES = {}
BUILD_NUMBER = "x"
MAIN_LOGGER = "main_logger"


SERVER_TYPE = os.environ.get("FLOW_SERVER_TYPE", "DEVELOPMENT")

if SERVER_TYPE == "HEROKU":
    # Heroku Settings
    import re
    import django_heroku
    django_heroku.settings(locals())

    DEBUG = os.environ.get("FLOW_DEBUG", "0") == "1"
    ALLOWED_HOSTS = os.environ.get("FLOW_ALLOWED_HOSTS", "").split(",")
    SECRET_KEY = os.environ.get("FLOW_DJANGO_SECRET_KEY")
    STATIC_URL = os.environ.get("FLOW_DJANGO_STATIC_URL")
    DJANGO_ADMIN_SITE_URL = os.environ.get("FLOW_DJANGO_ADMIN_SITE_URL")

    _build_number_matches = re.findall(r"\d+", os.environ.get("HEROKU_RELEASE_VERSION", "0"))
    if len(_build_number_matches) == 1:
        BUILD_NUMBER = _build_number_matches[0]

elif SERVER_TYPE == "TRAVIS":
    SECRET_KEY = "TRAVIS"
    STATIC_URL = "/static/"
    DJANGO_ADMIN_SITE_URL = "django_admin_site/"
    DATABASES = {
        'default': {
            'ENGINE':   'django.db.backends.postgresql_psycopg2',
            'NAME':     'travisci',
            'USER':     'postgres',
            'PASSWORD': '',
            'HOST':     'localhost',
            'PORT':     '',
        },
        'default': {
            'ENGINE':   'django.db.backends.postgresql_psycopg2',
            'NAME':     'travisci_test',
            'USER':     'postgres',
            'PASSWORD': '',
            'HOST':     'localhost',
            'PORT':     '',
        }
    }

else:
    # Local Settings
    import flow.local_settings as _settings

    DEBUG = _settings.DEBUG
    BASE_DIR = _settings.BASE_DIR
    SECRET_KEY = _settings.SECRET_KEY
    ALLOWED_HOSTS = _settings.ALLOWED_HOSTS
    DATABASES = _settings.DATABASES
    STATIC_URL = _settings.STATIC_URL
    DJANGO_ADMIN_SITE_URL = _settings.DJANGO_ADMIN_SITE_URL
    EASYJWT_SECRET_TOKEN = _settings.EASYJWT_SECRET_TOKEN


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': ('%(asctime)s [%(process)d] [%(levelname)s] ' +
                       'pathname=%(pathname)s lineno=%(lineno)s ' +
                       'funcname=%(funcName)s %(message)s'),
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'main_logger': {
            'handlers': ['console'],
            'level': 'INFO',
        }
    }
}
