
import json
import traceback
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


def api(func):
    @csrf_exempt
    def wrapper(request, *args, **kwargs):
        response_dict = {}
        # Set the json field
        try:
            setattr(request, "json", json.loads(request.body.decode("utf-8")))
        except:
            setattr(request, "json", {})
        # Process the request
        try:
            response_dict = func(request, *args, **kwargs)
        except Exception as e:
            traceback.print_exc()
            response_dict = { "success": False, }
        response = JsonResponse(response_dict)
        response["Access-Control-Allow-Origin"] = "*"
        return response
    return wrapper
