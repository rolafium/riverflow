
import os

DEBUG = False
ALLOWED_HOSTS = ["host"]
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'string'
EASYJWT_SECRET_TOKEN = 'string'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'flow_db_name',
        'USER': 'flow_db_user',
        'flow_db_password': 'flow_db_password',
        'HOST': 'localhost',
        'PORT': '',
    }
}

STATIC_URL = '/static/'
DJANGO_ADMIN_SITE_URL = 'admin/'
