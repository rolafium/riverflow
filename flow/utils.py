

class Visible:

    VISIBLE = 1
    ARCHIVED = 10
    REMOVED = 100

    VISIBILITY_CHOICES = (
        (VISIBLE, "Visible"),
        (ARCHIVED, "Archived"),
        (REMOVED, "Removed"),
    )

    @classmethod
    def visible(cls):
        return cls.objects.filter(visibility=cls.VISIBLE)

    @classmethod
    def archived(cls):
        return cls.objects.filter(visibility=cls.ARCHIVED)

    @classmethod
    def removed(cls):
        return cls.objects.filter(visibility=cls.REMOVED)

    def archive(self):
        self.visibility = self.ARCHIVED
        if(hasattr(self, "cascade")):
            self.cascade(self.ARCHIVED)

    def remove(self):
        self.visibility = self.REMOVED
        if(hasattr(self, "cascade")):
            self.cascade(self.REMOVED)



def uniform_url(url):
    return r"^URL$".replace("URL", url)
