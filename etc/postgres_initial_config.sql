
CREATE DATABASE flow_db_name;
CREATE USER flow_db_user WITH flow_db_password 'flow_db_password';
ALTER ROLE flow_db_user SET client_encoding TO 'utf8';
ALTER ROLE flow_db_user SET default_transaction_isolation TO 'read committed';
ALTER ROLE flow_db_user SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE flow_db_name TO flow_db_user;
