
from datetime import timedelta
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
from flow.utils import Visible
from flowing.exceptions import TooManyRequests


class FlowCategory(models.Model, Visible):

    name = models.CharField(max_length=255)
    is_global = models.BooleanField(default=False)
    users = models.ManyToManyField("auth.User")
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "is_global": self.is_global,
            "users": list(self.users.values("id", "email")),
        }

    def __str__(self):
        return self.name


class FlowStep(models.Model, Visible):

    UNDEFINED = 0
    BACKLOG = 100
    IN_PROGRESS = 200
    REVIEWING = 300
    COMPLETED = 400
    ABORTED = 500

    STATE_CHOICES = (
        (UNDEFINED, "Undefined"),
        (BACKLOG, "Backlog"),
        (IN_PROGRESS, "In Progress"),
        (REVIEWING, "Reviewing"),
        (COMPLETED, "Completed"),
        (ABORTED, "Aborted"),
    )

    name = models.CharField(max_length=255)
    category = models.ForeignKey("flowing.FlowCategory", on_delete=models.CASCADE)
    is_global = models.BooleanField(default=False)
    users = models.ManyToManyField("auth.User")
    index = models.IntegerField()
    state = models.IntegerField(default=UNDEFINED, choices=STATE_CHOICES)
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)


    def to_dict(self):
        return {
            "id": self.id,
            "category_id": self.category_id,
            "is_global": self.is_global,
            "users": list(self.users.values("id", "email")),
            "index": self.index,
            "state": self.state,
        }

    def __str__(self):
        return "{category} - {name}".format(
            category=self.category.name,
            name=self.name,
        )


class RiverPath(models.Model, Visible):
    name = models.CharField(max_length=255)
    is_global = models.BooleanField(default=False)
    users = models.ManyToManyField("auth.User")
    steps = models.ManyToManyField("flowing.FlowStep")
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)


    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "is_global": self.is_global,
            "users": list(self.users.values("id", "email")),
            "steps": list(self.steps.values_list("id", flat=True)),
        }

    def to_full_dict(self, request):
        response_dict = {}
        response_dict["river_path"] = self.to_dict()
        query = models.Q(is_global=True) | models.Q(users=request.user)
        response_dict["all_steps"] = list(FlowStep.objects.filter(query).values())
        return response_dict

    def __str__(self):
        return self.name


class River(models.Model, Visible):

    name = models.CharField(max_length=255)
    category = models.ManyToManyField("flowing.FlowCategory", blank=True)
    description = models.TextField(default="", blank=True)
    structure = models.ForeignKey("flowing.RiverPath", on_delete=models.SET_NULL, blank=True, null=True)
    users = models.ManyToManyField("auth.User")
    creation_date = models.DateTimeField(default=timezone.now)
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "category_id": list(self.users.values_list("id", flat=True)),
            "description": self.description,
            "structure_id": self.structure_id,
            "users": list(self.users.values("id", "email")),
        }

    def to_full_dict(self, request):
        response_dict = {}
        response_dict["river"] = self.to_dict()
        response_dict["structure"] = self.structure.to_full_dict(request)
        categories = response_dict["river"]["category_id"]
        response_dict["flow_targets"] = list(FlowTarget.objects.filter(river=self).values())
        response_dict["flow_stream_types"] = list(FlowStreamType.objects.filter(category__in=categories).values())
        response_dict["flow_streams"] = list(FlowStream.objects.filter(river=self).values())
        return response_dict

    def __str__(self):
        return self.name


class FlowTarget(models.Model, Visible):

    name = models.CharField(max_length=255)
    river = models.ForeignKey("flowing.River", on_delete=models.CASCADE)
    description = models.TextField(default="", blank=True)
    time = models.DateTimeField(default=timezone.now, blank=True, null=True)
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "river_id": self.river_id,
            "description": self.description,
            "time": str(self.time),
        }


    def __str__(self):
        return self.name


class FlowStreamType(models.Model, Visible):

    name = models.CharField(max_length=255)
    category = models.ForeignKey("flowing.FlowCategory", on_delete=models.CASCADE)
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "category_id": self.category_id,
        }

    def __str__(self):
        return "{category} - {name}".format(
            category=self.category.name,
            name=self.name,
        )


class FlowStream(models.Model, Visible):

    name = models.CharField(max_length=255)
    river = models.ForeignKey("flowing.River", on_delete=models.CASCADE)
    category = models.ForeignKey("flowing.FlowStreamType", on_delete=models.CASCADE)
    target = models.ForeignKey("flowing.FlowTarget", on_delete=models.CASCADE)
    description = models.TextField(default="", blank=True)
    state = models.ForeignKey("flowing.FlowStep", on_delete=models.CASCADE)
    creation_date = models.DateTimeField(default=timezone.now)
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "river_id": self.river_id,
            "category": self.category_id,
            "target": self.target_id,
            "description": self.description,
            "state": self.state_id,
            "creation_date": str(self.creation_date),
        }

    def __str__(self):
        return "{river} > {name} ({state})".format(
            river=self.river.name, 
            name=self.name, 
            state=self.state.name
        )

class FlowStreamComment(models.Model, Visible):

    name = models.CharField(max_length=255)
    stream = models.ForeignKey("flowing.FlowStream", on_delete=models.CASCADE)
    description = models.TextField(default="", blank=True)
    creation_date = models.DateTimeField(default=timezone.now)
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)


    @property
    def river(self):
        return self.stream.river

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "stream_id": self.stream_id,
            "description": self.description,
            "creation_date": str(self.creation_date),
        }

    def __str__(self):
        return "{stream} > {name}".format(
            stream=self.stream.name, 
            name=self.name,
        )



class UserProfile(models.Model, Visible):

    ANONYMOUS = 0
    UNREGISTRED = 100
    UNCONFIRMED_EMAIL = 200
    CONFIRMED_EMAIL = 300
    SUSPENDEND = 400

    STATE_CHOICES = (
        (ANONYMOUS, "Anonymous"),
        (UNREGISTRED, "Unregistred"),
        (UNCONFIRMED_EMAIL, "Unconfirmed email"),
        (CONFIRMED_EMAIL, "Confirmed email"),
        (SUSPENDEND, "Suspendend"),
    )

    user = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    state = models.IntegerField(default=UNREGISTRED, choices=STATE_CHOICES)
    visibility = models.IntegerField(default=Visible.VISIBLE, choices=Visible.VISIBILITY_CHOICES)

    def __str__(self):
        return "Profile {id} - {email}".format(
            id=self.user.id, 
            email=self.user.email,
        )


class UserRequest(models.Model):

    ip = models.CharField(max_length=40)
    area = models.CharField(max_length=64)
    request_method = models.CharField(max_length=10)
    request_path = models.TextField()
    request_time = models.DateTimeField(default=timezone.now)


    @classmethod
    def _get_ip_from_request(cls, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')

        return ip

    @classmethod
    def log(cls, request, area, limit=5):


        user_request = cls.objects.create(
            ip=cls._get_ip_from_request(request),
            area=area,
            request_method=request.method,
            request_path=request.build_absolute_uri()
        )

        if not request.user.is_superuser:
            cls.validate_request(request, area, limit)

        return user_request

    @classmethod
    def validate_request(cls, request, area, limit=5):

        time_from = timezone.now() - timedelta(minutes=settings.REQUEST_EXPIRE_MINUTES)
        logged_requests = cls.objects.filter(
            ip=cls._get_ip_from_request(request),
            area=area,
            request_time__gt=time_from,
        ).count()

        if logged_requests > limit:
            raise TooManyRequests(user_message="You raised too many requests.")


    def __str__(self):
        return "{ip} - {area}. {method}: {path}".format(
            ip=self.ip,
            area=self.area,
            method=self.request_method,
            path=self.request_path,
        )


def user_to_dict(user):
    return {
        "id": user.id,
        "username": user.username,
        "email": user.email,
        "first_name": user.first_name,
        "last_name": user.last_name,
    }


User.to_dict = user_to_dict
