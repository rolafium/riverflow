
from django.contrib import admin
from flowing.models import *


admin.site.register(River)
admin.site.register(RiverPath)
admin.site.register(FlowCategory)
admin.site.register(FlowStep)
admin.site.register(FlowTarget)
admin.site.register(FlowStream)
admin.site.register(FlowStreamComment)
admin.site.register(FlowStreamType)

admin.site.register(UserProfile)
admin.site.register(UserRequest)
