
from django.conf import settings

def versioning(request):
    version_context = {
        "VERSION": settings.VERSION,
        "BUILD_NUMBER": settings.BUILD_NUMBER,
        "DEBUG": settings.DEBUG,
    }
    return version_context
