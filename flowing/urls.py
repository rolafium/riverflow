
from django.urls import path, re_path
from flowing.views.public import *
from flowing.views.private import *


def _create_api_path(api_path, api_class):
    return path(api_path, api_class.as_view(), name=api_class.get_endpoint_name())

urlpatterns = [
    # API

    # Users block
    path("api/v1/users/register", flowing_api_v1_users_register, name="flowing_api_v1_users_register"),
    path("api/v1/users/login", flowing_api_v1_users_login, name="flowing_api_v1_users_login"),
    path("api/v1/users/logout", flowing_api_v1_users_logout, name="flowing_api_v1_users_logout"),
    path("api/v1/users/data", flowing_api_v1_users_data, name="flowing_api_v1_users_data"),

    path("api/v1/endpoints", flowing_api_v1_endpoints, name="flowing_api_v1_endpoints"),

    # Rivers block
    _create_api_path("api/v1/rivers", RiverApiView),
    _create_api_path("api/v1/rivers/<int:river_id>", RiverApiView),

    # Categories block
    _create_api_path("api/v1/flow-categories", FlowCategoryApiView),
    _create_api_path("api/v1/flow-categories/<int:flow_category_id>", FlowCategoryApiView),

    # Steps block
    _create_api_path("api/v1/flow-steps", FlowStepApiView),
    _create_api_path("api/v1/flow-steps/<int:flow_step_id>", FlowStepApiView),

    # Paths block
    _create_api_path("api/v1/river-paths", RiverPathApiView),
    _create_api_path("api/v1/river-paths/<int:river_path_id>", RiverPathApiView),

    # Targets block
    _create_api_path("api/v1/flow-targets", FlowTargetApiView),
    _create_api_path("api/v1/flow-targets/<int:flow_target_id>", FlowTargetApiView),

    # Stream types block
    _create_api_path("api/v1/flow-stream-types", FlowStreamTypeApiView),
    _create_api_path("api/v1/flow-stream-types/<int:flow_stream_type_id>", FlowStreamTypeApiView),

    # Streams block
    _create_api_path("api/v1/flow-streams", FlowStreamApiView),
    _create_api_path("api/v1/flow-streams/<int:flow_stream_id>", FlowStreamApiView),

    # Stream comments block
    _create_api_path("api/v1/flow-stream-comments", FlowStreamCommentApiView),
    _create_api_path("api/v1/flow-stream-comments/<int:flow_stream_comment_id>", FlowStreamCommentApiView),

    # Api invalid request block
    re_path("api/.*", flowing_api_error, name="flowing_api_error"),

    # Landing page
    re_path(".*", flowing_landing, name="flowing_landing"),
]
