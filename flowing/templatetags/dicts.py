
from django.template import Library

register = Library()


@register.filter(name="nice_word")
def nice_word(word):
    return word.replace("_", " ").capitalize()
