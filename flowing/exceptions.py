

class RequestException(Exception):
    ERROR_CODE = None
    ERROR_NAME = "Request Exception"

    def __init__(self, message="Error", user_message=None):
        self.message = message
        if user_message is not None:
            self.user_message = user_message
        else:
            self.user_message = self.ERROR_NAME

    def __str__(self):
        return "Error %s - %s: %s (%s)" % (self.ERROR_CODE, self.ERROR_NAME, self.message, self.user_message)




class InvalidRequest(RequestException):
    ERROR_CODE = 400
    ERROR_NAME = "Invalid Request"


class PermissionDenied(RequestException):
    ERROR_CODE = 403
    ERROR_NAME = "Permission Denied"


class NotFound(RequestException):
    ERROR_CODE = 404
    ERROR_NAME = "Not Found"


class TooManyRequests(RequestException):
    ERROR_CODE = 429
    ERROR_NAME = "Too Many Requests"


class InternalServerError(RequestException):
    ERROR_CODE = 500
    ERROR_NAME = "Internal Server Error"


