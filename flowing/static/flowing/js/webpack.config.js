const path = require("path");
const webpack = require("webpack");


const root = process.env.PWD;

module.exports = {
    devtool : "cheap-module-source-map",
    entry : root + "/src/main.js",
    mode: "development",
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
            }
        ]
    },
    output : {
        path : root + "/build",
        filename : "flow.js",
    },
    plugins: [],
}
