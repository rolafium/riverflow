
export default {
    state: {
        user: window.user,
        page: {
            h1: "Riverflow",
            h2: "Welcome to Riverflow",
        },
        forms: {},
    },
    mutations: {
        auth(state, value) {
            state.user.auth = value;
        },
        updateUser(state, data) {
            if(!data) {
                data = {};
            }
            state.user.id = data.id || 0;
            state.user.username = data.username || "";
            state.user.email = data.email || "";
            state.user.firstName = data.first_name || "";
            state.user.lastName = data.last_name || "";
        },
        addForm(state, form) {
            state.forms[form.formName] = form.data;
        },
    },
}
