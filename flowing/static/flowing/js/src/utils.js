

const populateEndpoint = (endpointName, values) => {
    let endpoint = window.ENDPOINTS[endpointName];
    if(values) {
        for(let i = 0; i < values.length; i++) {
            endpoint = endpoint.replace("0", values[i]);
        }
    }
    return endpoint;
}

const getCookie = (name) => {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) {
        return parts.pop().split(";").shift();
    } else {
        return "";
    }
}

const request = (endpoint, config) => {
    switch(config.preset) {
        case "json":
        case "JSON":
            config.body = JSON.stringify(config.body);
            config.headers = {
                "Accept": "application/json",
                "Content-Type": "application/json",
            };
            break;
    }

    if((config.method === "GET" || config.method === "DELETE") && config.body) {
        var body = config.body;
        var query = Object.keys(body)
            .map((key) => {
               return encodeURIComponent(key) + "=" + encodeURIComponent(body[key]);
            });
        endpoint += "?" + query
        delete config.body;
    }

    if(!config.credentials) {
        config.credentials = "same-origin";
    }
    
    if(!config.headers) {
        config.headers = {};
    }

    config.headers["X-CSRFToken"] = getCookie("csrftoken");

    var _success = true;
    var _status = 200;
    var _responseData = null;

    return new Promise((resolve, reject) => {
        window.fetch(endpoint, config)
        .then((response) => {
            var contentType = response.headers.get("Content-Type");
            var isJson = contentType.indexOf("application/json") > -1;
            var readMethod = isJson ? "json" : "text";

            _success = response.ok
            _status = response.status;

            return response[readMethod]();
        })
        .then((responseData) => {
            _responseData = responseData;
            if(_success) {
                if(window.system.DEBUG) {
                    console.warn("Ajax success", { endpoint, config, _responseData });
                }
                resolve(_responseData);
            } else {
                var error = "Response failed with status code: " + _status
                throw Error(error);
            }
        })
        .catch((error) => {
            if(window.system.DEBUG) {
                console.error("Ajax failure", { endpoint, config, error, _responseData });
            }
            reject(error);
        });
    });
}

const getData = (models) => {
    const url = populateEndpoint("USER_DATA");
    const config = {
        method: "GET",
        body: {
            models: models.join(","),
        }
    }
    return request(url, config);
}

const getForm = function(formName) {
    return new Promise((resolve, reject) => {
        if(this.$store.state.forms[formName]) {
            resolve(this.$store.state.forms[formName]);
        } else {
            const endpoint = populateEndpoint(formName)
            const config = {
                method: "OPTIONS",
                preset: "json",
            }

            request(endpoint, config)
            .then((data) => {
                delete data.status;
                this.$store.commit("addForm", { formName, data });
                resolve(data);
            })
            .catch((error) => reject(error));
        }
    });
}

export default {
    populateEndpoint,
    getCookie,
    request,
    getData,
    getForm,
};
