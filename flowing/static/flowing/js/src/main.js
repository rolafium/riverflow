

import Vue from "vue/dist/vue.esm.js";
import Vuex from "vuex";
import VueRouter from "vue-router";

import Utils from "./utils.js";
import storeConfig from "./storeConfig.js";
import routerConfig from "./routerConfig.js";

import App from "./vue/App.vue"


Vue.use(Vuex);
Vue.use(VueRouter);

Vue.prototype.request = Utils.request;
Vue.prototype.endpoint = Utils.populateEndpoint;
Vue.prototype.getData = Utils.getData;
Vue.prototype.getForm = Utils.getForm;

const store = new Vuex.Store(storeConfig);
const router = new VueRouter(routerConfig);

const app = new Vue({
    el: "#riverflow-root",
    components: { app: App, },
    store: store, 
    router: router,
});

if(window.system.DEBUG) {
    window.app = app;
    window.store = store;
    window.router = router;
    window.utils = Utils;
}
