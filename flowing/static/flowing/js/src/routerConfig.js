
const load = (component) => require(`./vue/${component}.vue`).default;

export default {
    mode: "history",
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        { path: "/", component: load("Landing"), name: "landing" },
        { path: "/flow-in", component: load("Unlogged"), name: "unlogged" },
        { path: "/home", component: load("Home"), name: "home" },
        { path: "/river/:id", component: load("River"), name: "river" },
        { path: "/structure/:id?", component: load("Structure"), name: "structure" },
        { path: "*", component: load("Landing"), name: "404" },
    ],
}
