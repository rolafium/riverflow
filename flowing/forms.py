import logging
from django import forms
from django.db.models import Q
from flowing.exceptions import InvalidRequest
from flowing.models import *


logger = logging.getLogger(settings.MAIN_LOGGER)


class ApiForm(forms.ModelForm):

    queries = {}

    def is_valid(self):
        if super().is_valid():
            return True
        else:
            logger.info(self.errors)
            raise InvalidRequest(user_message="Form is invalid.")

    @classmethod
    def get_structure(cls, request):
        _form_structure = {}
        for field_name in cls.base_fields.keys():
            field = cls.base_fields[field_name]
            field_structure = {
                "name": field_name,
                "nice_name": field_name.replace("_", " ").capitalize()
            }

            if isinstance(field, forms.fields.CharField):
                if hasattr(field, "max_length") and field.max_length:
                    field_structure["widget"] = "char"
                    field_structure["max_length"] = field.max_length
                else:
                    field_structure["widget"] = "text"
                field_structure["value"] = ""
                
            elif isinstance(field, forms.fields.BooleanField):
                field_structure["widget"] = "boolean"
                field_structure["value"] = False
                
            elif isinstance(field, forms.fields.IntegerField):
                field_structure["widget"] = "int"
                field_structure["value"] = ""
                
            elif isinstance(field, forms.fields.DateTimeField):
                field_structure["widget"] = "datetime"
                field_structure["value"] = ""
                
            elif isinstance(field, forms.fields.TypedChoiceField):
                field_structure["widget"] = "select"
                field_structure["options"] = field.choices
                field_structure["value"] = ""

            elif isinstance(field, forms.models.ModelChoiceField):
                field_structure["widget"] = "select"
                lambda_query = cls.queries.get(field_name, None)
                if lambda_query:
                    query = lambda_query(request)
                    field_structure["options"] = list(field.choices.queryset.filter(query).values())
                field_structure["value"] = ""

            if isinstance(field, forms.models.ModelMultipleChoiceField):
                field_structure["widget"] = "select"
                field_structure["multiple"] = True
                lambda_query = cls.queries.get(field_name, None)
                if lambda_query:
                    query = lambda_query(request)
                    field_structure["options"] = list(field.choices.queryset.filter(query).values())
                field_structure["value"] = []


            _form_structure[field_name] = field_structure
                        
        return _form_structure


class RiverForm(ApiForm):
    class Meta:
        model = River
        fields = (
            "name",
            "category",
            "description", 
            "structure", 
        )

    queries = {
        "category": lambda request: Q(is_global=True) | Q(users=request.user),
        "structure": lambda request: Q(is_global=True) | Q(users=request.user),
    }

    def post_save(self, request, river):
        river.users.add(request.user)

class FlowCategoryForm(ApiForm):
    class Meta:
        model = FlowCategory
        fields = (
            "name",
        )

    def post_save(self, request, flow_category):
        flow_category.users.add(request.user)


class FlowStepForm(ApiForm):
    class Meta:
        model = FlowStep
        fields = (
            "name",
            "category",
            "index",
            "state",
        )

    def post_save(self, request, flow_step):
        flow_step.users.add(request.user)



class RiverPathForm(ApiForm):
    class Meta:
        model = RiverPath
        fields = (
            "name",
            "steps",
        )

    def post_save(self, request, flow_path):
        flow_path.users.add(request.user)


class FlowTargetForm(ApiForm):
    class Meta:
        model = FlowTarget
        fields = (
            "name",
            "river",
            "description",
            "time",
        )

    queries = {
        "river": lambda request: Q(users=request.user),
    }

class FlowStreamTypeForm(ApiForm):
    class Meta:
        model = FlowStreamType
        fields = (
            "name",
            "category"
        )

    queries = {
        "category": lambda request: Q(users=request.user) | Q(is_global=True),
    }


class FlowStreamForm(ApiForm):
    class Meta:
        model = FlowStream
        fields = (
            "name",
            "river",
            "category",
            "target",
            "description",
            "state",
        )

    queries = {
        "river": lambda request: Q(users=request.user),
        "category": lambda request: Q(category__users=request.user) | Q(category__is_global=True),
        "target": lambda request: Q(river__users=request.user),
        "state": lambda request: Q(is_global=True) | Q(users=request.user),
    }


class FlowStreamCommentForm(ApiForm):
    class Meta:
        model = FlowStreamComment
        fields = (
            "name",
            "stream",
            "description",
        )
