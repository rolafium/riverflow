
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render
from flowing.decorators import flow_api
from flowing.exceptions import InvalidRequest, PermissionDenied
from flowing.models import UserProfile, UserRequest
from flowing.views.private import PrivateApi


def flowing_landing(request):
    context = {}
    context["private_endpoints"] = PrivateApi.get_name_url_pair()

    return render(request, "flowing/landing.html", context)

@flow_api
def flowing_api_v1_users_register(request):
    if request.user.is_authenticated:
        raise InvalidRequest(user_message="Only logged out users can register.")

    user_email = request.json.get("email", None)
    user_password = request.json.get("password", None)

    if not user_email or not user_password:
        raise InvalidRequest(user_message="Please provide an email and a password.")

    existing_user = User.objects.filter(username=user_email).exists()

    if existing_user:
        raise InvalidRequest(user_message="User %s already exists." % user_email)

    UserRequest.log(request, "users_register", limit=2)

    user = User.objects.create_user(user_email, user_email, user_password)
    user_profile = UserProfile.objects.create(user=user)
    login(request, user)

    response = {
        "login": True,
        "user": user.to_dict(),
    }

    return response

@flow_api
def flowing_api_v1_users_login(request):
    if request.user.is_authenticated:
        raise InvalidRequest(user_message="User is already authenticated.")

    user_email = request.json.get("email", None)
    user_password = request.json.get("password", None)

    if not user_email or not user_password:
        raise InvalidRequest("Please provide an emal and a password.")

    user = authenticate(request, username=user_email, password=user_password)

    if user is not None:
        login(request, user)
    else:
        raise PermissionDenied(user_message="Email or password are incorrect.")

    return {
        "login": True,
        "user": user.to_dict(),
    }

@flow_api
def flowing_api_v1_users_logout(request):
    if not request.user.is_authenticated:
        raise InvalidRequest(user_message="User isn't logged in.")

    logout(request)

    return {
        "login": False,
    }

@flow_api
def flowing_api_error(request):
    raise InvalidRequest(user_message="Invalid API endpoint.")
