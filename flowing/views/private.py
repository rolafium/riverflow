
from django.db.models import Q
from django.shortcuts import render
from flowing.decorators import flow_api
from flowing.views.abstract_views import ApiView
from flowing.forms import *
from flowing.models import *


class RiverApiView(ApiView):
    model = River
    form = RiverForm
    name = "river"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(users=request.user)


class FlowCategoryApiView(ApiView):
    model = FlowCategory
    form = FlowCategoryForm
    name = "flow_category"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(users=request.user) | Q(is_global=True)

class FlowStepApiView(ApiView):
    model = FlowStep
    form = FlowStepForm
    name = "flow_step"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(users=request.user) | Q(is_global=True)


class RiverPathApiView(ApiView):
    model = RiverPath
    form = RiverPathForm
    name = "river_path"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(users=request.user) | Q(is_global=True)


class FlowTargetApiView(ApiView):
    model = FlowTarget
    form = FlowTargetForm
    name = "flow_target"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(river__users=request.user)


class FlowStreamTypeApiView(ApiView):
    model = FlowStreamType
    form = FlowStreamTypeForm
    name = "flow_stream_type"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(category__users=request.user) | Q(category__is_global=True)


class FlowStreamApiView(ApiView):
    model = FlowStream
    form = FlowStreamForm
    name = "flow_stream"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(river__users=request.user)


class FlowStreamCommentApiView(ApiView):
    model = FlowStreamComment
    form = FlowStreamCommentForm
    name = "flow_stream_comment"

    @classmethod
    def _get_query(self, request, object_id):
        return Q(stream__river__users=request.user)


class PrivateApi:

    ENDPOINTS = [
        RiverApiView,
        FlowCategoryApiView,
        FlowStepApiView,
        RiverPathApiView,
        FlowTargetApiView,
        FlowStreamTypeApiView,
        FlowStreamApiView,
        FlowStreamCommentApiView,
    ]

    @classmethod
    def _view_dict(cls, view):
        return {
            "view": view,
            "name": view.name.upper(),
            "nice_name": view.name.replace("_", " ").title(),
            "url": view.get_url(),
        }

    @classmethod
    def to_dict(cls):
        api_dict = {}
        for view in cls.ENDPOINTS:
            api_dict[view.name] = cls._view_dict(view)
        return api_dict


    @classmethod
    def get_name_url_pair(cls):
        pairs = []
        for view in cls.ENDPOINTS:
            pairs.append(cls._view_dict(view))
        return pairs


@flow_api
def flowing_api_v1_users_data(request):
    api_dict = PrivateApi.to_dict()
    api_dict_keys = api_dict.keys()
    data = {}
    requested_models = request.GET.get("models", "").split(",")
    for requested_model in requested_models:
        if requested_model in api_dict_keys:
            api_view = api_dict[requested_model]["view"]
            query = api_view._get_query(request, 0)
            data[requested_model] = list(api_view.model.objects.filter(query).values(**api_view.values))

    return data



def flowing_api_v1_endpoints(request):
    context = {
        "endpoints": PrivateApi.get_name_url_pair(),
    }
    return render(request, "flowing/api/endpoints.html", context)
