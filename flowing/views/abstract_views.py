
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.generic import View
from django.urls import reverse
from django.utils.decorators import method_decorator
from flowing.decorators import flow_api, flow_login_required
from flowing.exceptions import InvalidRequest, PermissionDenied, NotFound


class ApiView(View):
    model = None
    form = None
    name = "object"
    values = {}

    @classmethod
    def get_id_name(self):
        return self.name + "_id"

    @classmethod
    def get_endpoint_name(cls):
        return "flowing_api_v{api_version}_{name}".format(
            api_version=1,
            name=cls.name,
        )

    @classmethod
    def get_url(cls, kwargs={}):
        return reverse(cls.get_endpoint_name(), kwargs=kwargs)

    @classmethod
    def _get_query(cls, request, object_id, *args, **kwargs):
        return Q()

    @classmethod
    def _get_objects(cls, request, object_id, *args, **kwargs):
        query = cls._get_query(request, object_id, *args, **kwargs)
        objs = cls.model.visible().filter(query)
        return objs


    @classmethod
    def _get_object(cls, request, object_id, *args, **kwargs):
        try:
            query = cls._get_query(request, object_id, *args, **kwargs) & Q(pk=object_id)
            obj = cls.model.visible().get(query)
            return obj
        except cls.model.DoesNotExist:
            raise NotFound(user_message="Object not found")


    def _validate_method(self, request, object_id):
        if request.method == "GET" or request.method == "POST" or request.method == "DELETE":
            if object_id == 0:
                raise InvalidRequest(user_message="Cannot GET, POST or PUT to unexisting object.")

        elif request.method == "PUT":
            if object_id != 0:
                raise InvalidRequest(user_message="Cannot PUT on existing object.")

        else:
            raise InvalidRequest(user_message="Invalid request method.")

    @method_decorator([flow_api, flow_login_required])
    def options(self, request, **kwargs):
        structure = self.form.get_structure(request)
        return structure

    def get(self, request, **kwargs):
        if request.GET.get("html", None) is not None:
            return self.get_html(request, **kwargs)
        else:
            return self.get_json(request, **kwargs)

    @method_decorator(login_required)
    def get_html(self, request, **kwargs):
        object_id = kwargs.get(self.get_id_name(), 0)
        context = {
            "api_view_class": self,
            "fields": self.model._meta.get_fields(),
        }
        template = ""

        if object_id == 0:
            objs = self._get_objects(request, object_id)
            context["data"] = objs
            template = "flowing/api/list.html"
        else:
            obj = self._get_object(request, object_id)
            context["data"] = obj
            template = "flowing/api/detail.html"

        return render(request, template, context)

    @method_decorator([flow_api, flow_login_required])
    def get_json(self, request, **kwargs):
        object_id = kwargs.get(self.get_id_name(), 0)
        data = {}
        if object_id == 0:
            objs = self._get_objects(request, object_id)
            data = {
                "objects": list(objs.values(**self.values))
            }
        else:
            data = {}
            obj = self._get_object(request, object_id)
            get_full_dict = request.GET.get("full", None)
            if get_full_dict is not None and hasattr(obj, "to_full_dict"):
                data = obj.to_full_dict(request)
            else:
                data = obj.to_dict()
        return data


    @method_decorator([flow_api, flow_login_required])
    def put(self, request, **kwargs):
        form = self.form(request.json)

        if form.is_valid():
            obj = form.save()
            if hasattr(form, "post_save"):
                form.post_save(request, obj)
            return obj.to_dict()


    @method_decorator([flow_api, flow_login_required])
    def post(self, request, **kwargs):
        object_id = kwargs[self.get_id_name()]
        self._validate_method(request, object_id)
        obj = self._get_object(request, object_id)

        if hasattr(obj, "is_global") and obj.is_global and not request.user.is_superuser:
            raise PermissionDenied("Only superusers can edit a global object.")

        form = self.form(request.json, instance=obj)

        if form.is_valid():
            obj = form.save()
            if hasattr(form, "post_save"):
                form.post_save(request, obj)
            return obj.to_dict()


    @method_decorator([flow_api, flow_login_required])
    def delete(self, request, **kwargs):
        object_id = kwargs[self.get_id_name()]
        self._validate_method(request, object_id)
        obj = self._get_object(request, object_id)
        obj.delete()

        response = {
            "deleted": True,
        }

        return response


    def __str__(self):
        return self.name.replace("_", " ").title()
