
import logging
import traceback
from django.conf import settings
from django.http import JsonResponse
from flowing.exceptions import InvalidRequest, PermissionDenied, NotFound, TooManyRequests, InternalServerError

logger = logging.getLogger(settings.MAIN_LOGGER)

def flow_api(func):

    def wrapper(request, *args, **kwargs):

        status = 200
        response_dict = {}
        try:
            response_dict = func(request, *args, **kwargs)
        except (InvalidRequest, PermissionDenied, TooManyRequests, NotFound) as e:
            logger.debug(e)
            status = e.ERROR_CODE
            response_dict["error"] = e.user_message
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            status = InternalServerError.ERROR_CODE
            response_dict["error"] = InternalServerError.ERROR_NAME

        response_dict["status"] = status
        response = JsonResponse(response_dict, status=status)
        return response

    return wrapper


def flow_login_required(func):

    def wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise PermissionDenied
        return func(request, *args, **kwargs)

    return wrapper
