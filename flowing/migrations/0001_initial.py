# Generated by Django 2.0.3 on 2018-05-26 21:35

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import flow.utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FlowCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('is_global', models.BooleanField(default=False)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='FlowStep',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('is_global', models.BooleanField(default=False)),
                ('index', models.IntegerField()),
                ('state', models.IntegerField(choices=[(0, 'Undefined'), (100, 'Backlog'), (200, 'In Progress'), (300, 'Reviewing'), (400, 'Completed'), (500, 'Aborted')], default=0)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.FlowCategory')),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='FlowStream',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, default='')),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='FlowStreamComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, default='')),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
                ('stream', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.FlowStream')),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='FlowStreamType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.FlowCategory')),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='FlowTarget',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, default='')),
                ('time', models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='River',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, default='')),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
                ('category', models.ManyToManyField(blank=True, to='flowing.FlowCategory')),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='RiverPath',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('is_global', models.BooleanField(default=False)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
                ('steps', models.ManyToManyField(to='flowing.FlowStep')),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', models.IntegerField(choices=[(0, 'Anonymous'), (100, 'Unregistred'), (200, 'Unconfirmed email'), (300, 'Confirmed email'), (400, 'Suspendend')], default=100)),
                ('visibility', models.IntegerField(choices=[(1, 'Visible'), (10, 'Archived'), (100, 'Removed')], default=1)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            bases=(models.Model, flow.utils.Visible),
        ),
        migrations.CreateModel(
            name='UserRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip', models.CharField(max_length=40)),
                ('area', models.CharField(max_length=64)),
                ('request_method', models.CharField(max_length=10)),
                ('request_path', models.TextField()),
                ('request_time', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.AddField(
            model_name='river',
            name='structure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='flowing.RiverPath'),
        ),
        migrations.AddField(
            model_name='river',
            name='users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='flowtarget',
            name='river',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.River'),
        ),
        migrations.AddField(
            model_name='flowstream',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.FlowStreamType'),
        ),
        migrations.AddField(
            model_name='flowstream',
            name='river',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.River'),
        ),
        migrations.AddField(
            model_name='flowstream',
            name='state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.FlowStep'),
        ),
        migrations.AddField(
            model_name='flowstream',
            name='target',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flowing.FlowTarget'),
        ),
    ]
