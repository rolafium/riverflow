from flowing.models import UserProfile
import json

class UserProfileMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            profile, created = UserProfile.objects.get_or_create(user_id=request.user.id)
            request.profile = profile
        else:
            request.profile = UserProfile(state=UserProfile.ANONYMOUS)

        response = self.get_response(request)

        return response

class JsonMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        request.json = {}
        if request.META.get("HTTP_ACCEPT", "") == "application/json":
            try:
                request.json = json.loads(request.body)
            except ValueError:
                pass

        response = self.get_response(request)

        return response
