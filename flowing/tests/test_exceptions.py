from django.test import TestCase
from flowing.exceptions import *


class ExceptionTestCase(TestCase):

    def _raise_exception(self, exception_class, **kwargs):
        try:
            raise exception_class(**kwargs)
        except exception_class as e:
            return e

    def _run_exception_test(self, exception_class, error_code, user_message):
        exception = self._raise_exception(exception_class, user_message=user_message)

        self.assertTrue(hasattr(exception, "ERROR_CODE"))
        self.assertTrue(hasattr(exception, "ERROR_NAME"))
        self.assertTrue(hasattr(exception, "user_message"))
        self.assertEqual(exception.ERROR_CODE, error_code)
        self.assertEqual(exception.user_message, user_message)


    def test_invalid_request(self):
        self._run_exception_test(InvalidRequest, 400, "Test Invalid Request")

    def test_permission_denied(self):
        self._run_exception_test(PermissionDenied, 403, "Test Permission Denied")

    def test_not_found(self):
        self._run_exception_test(NotFound, 404, "Test Not Found")

    def test_too_many_requests(self):
        self._run_exception_test(TooManyRequests, 429, "Test Too Many Requests")

    def test_internal_server_error(self):
        self._run_exception_test(InternalServerError, 500, "Test Internal Server Error")
