import random
import string
from django.forms.fields import *
from django.forms.models import *


def populate_form(form):
    _form_data = {}
    for field_name in form.fields.keys():
        field = form.fields[field_name]
        if field.required:
            _field_value = None
            
            if isinstance(field, CharField):
                _max_length = 100
                if hasattr(field, "max_length"):
                    _max_length = random.randint(1, field.max_length)

                _field_array = []
                for i in range(_max_length):
                    _field_array.append(random.choice(string.ascii_letters))

                _field_value = "".join(_field_array)
                
            elif isinstance(field, BooleanField):
                _field_value = not not random.randint(0, 1)
                
            elif isinstance(field, IntegerField):
                _max_value = field.max_value if field.max_value else 100
                _field_value = random.randint(0, _max_value)
                
            elif isinstance(field, DateTimeField):
                _field_value = str(timezone.now())
                
            elif isinstance(field, TypedChoiceField):
                _field_value = field.choices[0][0]

            elif isinstance(field, ModelChoiceField):
                _field_value = field.choices.queryset.first().id
                
            if isinstance(field, ModelMultipleChoiceField):
                _field_value = [ field.choices.queryset.first().id ]
                
            _form_data[field_name] = _field_value
        
    return _form_data


