import json
from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse
from flowing.models import UserRequest


REGISTER_COUNT = 0
POST_HEADERS = {
    "HTTP_ACCEPT": "application/json",
    "content_type": "application/json",
}

class PublicApiTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.email = "jbitshine+public_api_test@gmail.com"
        self.password = "dummy_password"
        UserRequest.objects.all().delete()
        User.objects.filter(email__startswith=self.email).delete()

    def _get_email(self):
        global REGISTER_COUNT
        REGISTER_COUNT += 1
        email = self.email + str(REGISTER_COUNT)
        return email

    def _register(self, body=None):
        url = reverse("flowing_api_v1_users_register")
        if body is None:
            body = {
                "email": self._get_email(),
                "password": self.password,
            }

        response = self.client.post(url, json.dumps(body), **POST_HEADERS)
        response_data = json.loads(response.content)

        return response_data

    def _login(self, email, password):
        url = reverse("flowing_api_v1_users_login")
        body = {
            "email": email,
            "password": password,
        }
        response = self.client.post(url, json.dumps(body), **POST_HEADERS)
        response_data = json.loads(response.content)

        return response_data

    def _logout(self):
        url = reverse("flowing_api_v1_users_logout")
        response = self.client.get(url, **POST_HEADERS)
        response_data = json.loads(response.content)

        return response_data

    def test_register(self):
        # Bad register
        body = {
            "email": self._get_email()
        }
        response_data = self._register(body)
        self.assertEqual(response_data["status"], 400)

        # Good register
        response_data = self._register()
        self.assertEqual(response_data["status"], 200)
        self.assertEqual(response_data["login"], True)
        self.assertTrue(self.email in response_data["user"]["email"])

        # Can't register when logged in
        response_data = self._register()
        self.assertEqual(response_data["status"], 400)

        # Spam registers
        for i in range(10):
            response_data = self._register()

        self.assertTrue(response_data["status"], 429)

    def test_login_and_logout(self):
        email = self._get_email()
        User.objects.create_user(email, email, self.password)

        # Login
        response_data = self._login(email, self.password)
        self.assertEqual(response_data["status"], 200)
        self.assertEqual(response_data["login"], True)
        self.assertEqual(response_data["user"]["email"], email)

        # Can't login if you are already logged in
        response_data = self._login(email, self.password)
        self.assertTrue(response_data["status"] != 200)

        # Logout
        response_data = self._logout()
        self.assertEqual(response_data["status"], 200)
        self.assertEqual(response_data["login"], False)

        # Can't logout if you aren't logged in
        response_data = self._logout()
        self.assertTrue(response_data["status"] != 200)
