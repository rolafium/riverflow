import json
from datetime import datetime
from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse
from django.utils import timezone
from flowing.models import *
from flowing.views.private import *
from flowing.tests.utils import populate_form


POST_HEADERS = {
    "HTTP_ACCEPT": "application/json",
    "content_type": "application/json",
}

class PrivateApiTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.email = "jbitshine+private_api_test@gmail.com"
        cls.password = "dummy_password"
        cls.user = User.objects.create_user(username=cls.email, password=cls.password)


        # Create one of each so we have a populated database 
        # for the random foreign key & many to many fields
        def _create_from_view(api_view_class, add_user=False):
            data = populate_form(api_view_class.form())
            obj = api_view_class.form(data).save()
            if add_user:
                obj.users.add(cls.user)

        _create_from_view(FlowCategoryApiView, add_user=True)
        _create_from_view(FlowStepApiView, add_user=True)
        _create_from_view(FlowStreamTypeApiView)
        _create_from_view(RiverPathApiView, add_user=True)
        _create_from_view(RiverApiView, add_user=True)
        _create_from_view(FlowTargetApiView)
        _create_from_view(FlowStreamApiView)
        _create_from_view(FlowStreamCommentApiView)

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.client = Client()
        self.client.login(username=self.email, password=self.password)

    def _check_object_json_data(self, model, data):
        self.assertEqual(data["status"], 200)
        obj = model.objects.get(pk=data["id"])
        obj_dict = obj.to_dict()
        for key in obj_dict:
            # Skip the datetime field as it won't be formatted the same
            if hasattr(obj, key) and type(getattr(obj, key)) == datetime:
                continue
            self.assertEqual(obj_dict[key], data[key])

    def _test_endpoint(self, api_view_class):

        _view_name = api_view_class.get_endpoint_name()
        _view_arg_name = api_view_class.get_id_name()
        _model = api_view_class.model
        _fake_id = 9999

        # GET 404 BEFORE PUT
        url = reverse(_view_name, kwargs={ _view_arg_name: _fake_id })
        response = self.client.get(url)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], 404)

        # POST 404 BEFORE PUT
        body = populate_form(api_view_class.form())
        response = self.client.post(url, json.dumps(body), **POST_HEADERS)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], 404)

        # DELETE 404 BEFORE PUT
        response = self.client.delete(url)
        response_data = json.loads(response.content)
        self.assertTrue(response_data["status"], 404)

        # LIST VIEW GET 200
        # As we know we have at least 1 object
        # created at the beginning of the test case
        url = reverse(_view_name)
        response = self.client.get(url)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], 200)
        self.assertTrue(len(response_data["objects"]) > 0)

        # PUT 200
        body = populate_form(api_view_class.form())
        response = self.client.put(url, json.dumps(body), **POST_HEADERS)
        response_data = json.loads(response.content)
        self._check_object_json_data(_model, response_data)

        _created_obj_id = response_data["id"]

        # POST 200 AFTER PUT
        url = reverse(_view_name, kwargs={ _view_arg_name: _created_obj_id })
        body = populate_form(api_view_class.form())
        response = self.client.post(url, json.dumps(body), **POST_HEADERS)
        response_data = json.loads(response.content)
        self._check_object_json_data(_model, response_data)

        # GET 200 AFTER PUT
        response = self.client.get(url)
        response_data = json.loads(response.content)
        self._check_object_json_data(_model, response_data)

        # DELETE 200 AFTER PUT
        response = self.client.delete(url)
        response_data = json.loads(response.content)
        self.assertTrue(response_data["status"], 200)
        self.assertTrue(response_data["deleted"])

        # GET 404 AFTER DELETE
        response = self.client.get(url)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], 404)


    def test_river_views(self):
        self._test_endpoint(RiverApiView)

    def test_category_views(self):
        self._test_endpoint(FlowCategoryApiView)

    def test_step_views(self):
        self._test_endpoint(FlowStepApiView)

    def test_path_views(self):
        self._test_endpoint(RiverPathApiView)

    def test_target_views(self):
        self._test_endpoint(FlowTargetApiView)

    def test_stream_type_views(self):
        self._test_endpoint(FlowStreamTypeApiView)

    def test_stream_views(self):
        self._test_endpoint(FlowStreamApiView)

    def test_stream_comment_views(self):
        self._test_endpoint(FlowStreamCommentApiView)


    def test_users_data(self):
        url = reverse("flowing_api_v1_users_data")

        data = {}
        response = self.client.get(url, data)
        response_data = json.loads(response.content)
        self.assertDictEqual(response_data, { "status": 200 })

        requested_model = "flow_category"
        data = {
            "models": requested_model
        }
        response = self.client.get(url, data)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], 200)
        self.assertTrue(requested_model in response_data)

        requested_models = [
            "flow_category",
            "flow_step",
            "this_wont_show_up",
            "river",
            "flow_target"
        ]
        unfound_model = "this_wont_show_up"

        data = {
            "models": ",".join(requested_models)
        }
        response = self.client.get(url, data)
        response_data = json.loads(response.content)
        self.assertEqual(response_data["status"], 200)

        for requested_model in requested_models:
            found = requested_model in response_data
            if requested_model == unfound_model:
                self.assertTrue(not found)
            else:
                self.assertTrue(found)



