#!/bin/sh


# Import the enviroment variables
. bin/env.sh

# Go to the javascript root
cd $FLOW_JAVASCRIPT_ROOT

# Run webpack
webpack --watch

# Back to the original folder
cd $FLOW_PROJECT_ROOT
