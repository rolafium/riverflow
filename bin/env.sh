#!/bin/sh


# Project Root
export FLOW_PROJECT_ROOT=`pwd`

# Static Root
export FLOW_STATIC_ROOT="$FLOW_PROJECT_ROOT/flowing/static/flowing"

# CSS Root
export FLOW_CSS_ROOT="$FLOW_STATIC_ROOT/css"

# JavaScript Root
export FLOW_JAVASCRIPT_ROOT="$FLOW_STATIC_ROOT/js"
