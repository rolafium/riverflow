#!/bin/sh


# Import the enviroment variables
. bin/env.sh

TEST_MODULES="flowing.tests"

# Run the development server
python manage.py test $TEST_MODULES --traceback --verbosity 2
