#!/bin/sh



# Import the enviroment variables
. bin/env.sh

# Runs migrations
python manage.py migrate

# Go to the javascript root
cd $FLOW_JAVASCRIPT_ROOT
npm install

# Run webpack
node_modules/.bin/webpack-cli -p

# Back to the original folder
cd $FLOW_PROJECT_ROOT

# Collect statics
python manage.py collectstatic --noinput -i node_modules
